import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import todoApp from './reducers'
import App from './components/App'
import {addMessage} from './actions'

let store = createStore(todoApp)

render(
  <Provider store={store}>
      <App/>
  </Provider>,
  document.getElementById('root')
)
//random fire new message each 1 second
let i = 0;
let ranMes = ['Hi there!', 'Hi, How are you?', 'Not bad! What about you?', "Thank! I'm feeling so good.", 'Good bye!'];
setInterval(()=>{
    let msgId = Math.floor(Math.random() * 10)%5;
    store.dispatch(addMessage(++i + ': ' + ranMes[msgId]));
}, 1000)
