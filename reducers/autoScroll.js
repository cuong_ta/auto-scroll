const autoScroll = (state = true, action) => {
  switch (action.type) {
    case 'TOGGLE_AUTO':
      return !state;
    case 'UPDATE_AUTO':
      console.log('Run through update auto: ' + state);
      return action.value;
    default:
      return state
  }
}

export default autoScroll
