import React from 'react'
import ReactDOM from 'react-dom'
import {changeAuto} from '../actions'
import {connect} from 'react-redux'
import onClickOutside from 'react-onclickoutside';

class AutoScroll extends React.Component {
    constructor(props){
        super(props);
        this.lastScrollTop = 0;
    }
    componentDidUpdate() {
        if(this.props.autoScroll) {
            this.refs.container.scrollTop = this.refs.container.scrollHeight - this.refs.container.clientHeight;
            this.lastScrollTop = this.refs.container.scrollTop;
        }
    }
    render() {
        return <div style={this.props.style} ref="container" onScroll={this.onScroll.bind(this)}>
            {this.props.children}
        </div>
    }
    onScroll(e){
        let maxScrollTop = this.refs.container.scrollHeight - this.refs.container.clientHeight;
        //if scroll down to bottom, we continue auto scroll (tolerance 10px)
        //otherwise, set auto scroll should be disable
        this.props.changeAuto(
            this.refs.container.scrollTop >= this.lastScrollTop &&
            this.refs.container.scrollTop + 10 >= maxScrollTop
        );
        console.log(this.refs.container.scrollTop >= this.lastScrollTop &&
            this.refs.container.scrollTop >= maxScrollTop);

    }
    handleClickOutside(){
        console.log('Go outside');
        this.props.changeAuto(false);
    }
}

const mapStateToProps = (state) => {
    return {
        autoScroll: state.autoScroll
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changeAuto: (val) => {
            dispatch(changeAuto(val));
        }
    }
}
AutoScroll = onClickOutside(AutoScroll);
AutoScroll = connect(mapStateToProps, mapDispatchToProps)(AutoScroll);
export default AutoScroll
