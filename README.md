#React auto scroll component
##What it does
You can use as auto scroll container for chat application
##Usage
```html
<AutoScroll style={{
height: '150px',
border: '1px solid',
overflow: 'auto'
}}>
    <ul>
        {this.props.lines.map((line, index) => (
            <li key={index}>{line.text}</li>
        ))}
    </ul>
</AutoScroll >
```
##Example & Install
```
npm install
npm start
//Open your browser at http://localhost:3000/ and enjoy
```


