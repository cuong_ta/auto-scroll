let nextMessageId = 0
export const addMessage = (text) => {
  return {
    type: 'ADD_MESSAGE',
    id: nextMessageId++,
    text
  }
}

export const changeAuto = (val) => {
  return {
    type: 'UPDATE_AUTO',
    value: val
  }
}
