import React from 'react'
import AutoScroll from './AutoScroll'
import {connect} from 'react-redux'
import {addMessage} from '../actions'

class App extends React.Component {
    render() {
        return (
            <div>
                <AutoScroll style={{
                height: '150px',
                border: '1px solid',
                overflow: 'auto'
              }}>
                    <ul>
                        {this.props.lines.map((line, index) => (
                            <li key={index}>{line.text}</li>
                        ))}
                    </ul>

                </AutoScroll >
                <p>Auto scroll status:&nbsp;
                    <span style={{color: this.props.autoScroll ? 'green' : 'red'}}>
                        {this.props.autoScroll ? 'enable' : 'disable'}
                    </span>
                </p>
                <p>You can 'scroll to bottom' to enable auto scroll</p>
                <p>You can 'click outside' or 'scroll up' to disable auto scroll</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        lines: state.messages,
        autoScroll: state.autoScroll
    }
}

App = connect(mapStateToProps)(App);

export default App
