import { combineReducers } from 'redux'
import messages from './messages'
import autoScroll from './autoScroll'

const todoApp = combineReducers({
  messages,
  autoScroll
})

export default todoApp
